import { OnInit } from '@angular/core';
import { ISelection } from './../../shared/interfaces/selection.interface';
import { IResult } from 'src/app/shared/interfaces/result.interface';

export abstract class BasePageComponent implements OnInit {
    public dropdowns: Array<any>;
    public inputNumber = 1;
    public selectedDropdown: any;
    public results: Array<IResult> = [];

    abstract onChange(data: ISelection): void;

    constructor() { }

    ngOnInit(): void {
        this.onChange({ inputNumber: this.inputNumber, selectedDropdown: this.selectedDropdown });
    }
}
