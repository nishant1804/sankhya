import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AreaComponent } from './components/area/area.component';
import { DataTransferRateComponent } from './components/data-transfer-rate/data-transfer-rate.component';
import { DigitalStorageComponent } from './components/digital-storage/digital-storage.component';
import { EnergyComponent } from './components/energy/energy.component';
import { FrequencyComponent } from './components/frequency/frequency.component';
import { FuelEconomyComponent } from './components/fuel-economy/fuel-economy.component';
import { LengthComponent } from './components/length/length.component';
import { MassComponent } from './components/mass/mass.component';
import { PlaneAngleComponent } from './components/plane-angle/plane-angle.component';
import { PressureComponent } from './components/pressure/pressure.component';
import { SpeedComponent } from './components/speed/speed.component';
import { TempratureComponent } from './components/temprature/temprature.component';
import { TimeComponent } from './components/time/time.component';
import { VolumeComponent } from './components/volume/volume.component';

export enum SCRouterLinks {
  Home = 'home',
  Area = 'home/area',
  DataTransferRate = 'home/dataTransferRate',
  DigitalStorage = 'home/digitalStorage',
  Energy = 'home/energy',
  Frequency = 'home/frequency',
  FuelEconomy = 'home/fuelEconomy',
  Length = 'home/length',
  Mass = 'home/mass',
  PlaneAngle = 'home/planeAngle',
  Pressure = 'home/pressure',
  Speed = 'home/speed',
  Temprature = 'home/temprature',
  Time = 'home/time',
  Volume = 'home/volume'
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {
        path: SCRouterLinks.Home,
        component: HomePage
      },
      {
        path: SCRouterLinks.Area,
        component: AreaComponent
      },
      {
        path: SCRouterLinks.DataTransferRate,
        component: DataTransferRateComponent
      },
      {
        path: SCRouterLinks.DigitalStorage,
        component: DigitalStorageComponent
      },
      {
        path: SCRouterLinks.Energy,
        component: EnergyComponent
      },
      {
        path: SCRouterLinks.Frequency,
        component: FrequencyComponent
      },
      {
        path: SCRouterLinks.FuelEconomy,
        component: FuelEconomyComponent
      },
      {
        path: SCRouterLinks.Length,
        component: LengthComponent
      },
      {
        path: SCRouterLinks.Mass,
        component: MassComponent
      },
      {
        path: SCRouterLinks.PlaneAngle,
        component: PlaneAngleComponent
      },
      {
        path: SCRouterLinks.Pressure,
        component: PressureComponent
      },
      {
        path: SCRouterLinks.Speed,
        component: SpeedComponent
      },
      {
        path: SCRouterLinks.Temprature,
        component: TempratureComponent
      },
      {
        path: SCRouterLinks.Time,
        component: TimeComponent
      },
      {
        path: SCRouterLinks.Volume,
        component: VolumeComponent
      }
    ])
  ],
  declarations: [
    HomePage,
    AreaComponent,
    DataTransferRateComponent,
    DigitalStorageComponent,
    EnergyComponent,
    FrequencyComponent,
    FuelEconomyComponent,
    LengthComponent,
    MassComponent,
    PlaneAngleComponent,
    PressureComponent,
    SpeedComponent,
    TempratureComponent,
    TimeComponent,
    VolumeComponent,
  ],
})
export class HomePageModule { }
