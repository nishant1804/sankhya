import { TileNames } from './../shared/enums/tiles.enum';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter, pairwise } from 'rxjs/operators';
import { ITile, LocalStorageService } from '../shared/services/local-storage.service';
import { SCRouterLinks } from './home.module';

@Component({
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  tiles: Array<ITile> = [];

  constructor(private router: Router, private localStorageService: LocalStorageService) {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd), pairwise())
      .subscribe((urls: [NavigationEnd, NavigationEnd]) => {
        if (urls[0].url === '/settings') {
          this.init();
        }
      });
  }

  ngOnInit(): void {
    this.init();
  }

  private init(): void {
    const tiles = this.localStorageService.tiles;
    if (tiles) {
      tiles.map((t: ITile) => {
        this.mapUrl(t);
      });
      this.tiles = (tiles && tiles.filter((t) => t.selected)) || [];
    }
  }

  private mapUrl(tile: ITile): void {
    switch (tile.name) {
      case TileNames.Area:
        tile.routerLink = SCRouterLinks.Area;
        break;
      case TileNames.DataTransferRate:
        tile.routerLink = SCRouterLinks.DataTransferRate;
        break;
      case TileNames.DigitalStorage:
        tile.routerLink = SCRouterLinks.DigitalStorage;
        break;
      case TileNames.Energy:
        tile.routerLink = SCRouterLinks.Energy;
        break;
      case TileNames.Frequency:
        tile.routerLink = SCRouterLinks.Frequency;
        break;
      case TileNames.FuelEconomy:
        tile.routerLink = SCRouterLinks.FuelEconomy;
        break;
      case TileNames.Length:
        tile.routerLink = SCRouterLinks.Length;
        break;
      case TileNames.Mass:
        tile.routerLink = SCRouterLinks.Mass;
        break;
      case TileNames.PlaneAngle:
        tile.routerLink = SCRouterLinks.PlaneAngle;
        break;
      case TileNames.Pressure:
        tile.routerLink = SCRouterLinks.Pressure;
        break;
      case TileNames.Speed:
        tile.routerLink = SCRouterLinks.Speed;
        break;
      case TileNames.Temprature:
        tile.routerLink = SCRouterLinks.Temprature;
        break;
      case TileNames.Time:
        tile.routerLink = SCRouterLinks.Time;
        break;
      case TileNames.Volume:
        tile.routerLink = SCRouterLinks.Volume;
        break;
      default:
        break;
    }
  }
}
