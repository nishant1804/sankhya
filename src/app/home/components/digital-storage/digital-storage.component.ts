import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from '../../utils/sc-base-page';
import { UtilService } from 'src/app/shared/services/util.service';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';

enum DigitalStorages {
  Bit = 'Bit',
  KiloBit = 'Kilo Bit',
  MegaBit = 'Mega Bit',
  GigaBit = 'Giga Bit',
  TeraBit = 'Tera Bit',
  PetaBit = 'Peta Bit',
  Byte = 'Byte',
  KiloByte = 'Kilo Byte',
  MegaByte = 'Mega Byte',
  GigaByte = 'Giga Byte',
  TeraByte = 'Tera Byte',
  PetaByte = 'Peta Byte',
}

@Component({
  templateUrl: './digital-storage.component.html',
  styleUrls: ['./digital-storage.component.scss'],
})
export class DigitalStorageComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = DigitalStorages.Bit;

    this.dropdowns = [
      DigitalStorages.Bit,
      DigitalStorages.KiloBit,
      DigitalStorages.MegaBit,
      DigitalStorages.GigaBit,
      DigitalStorages.TeraBit,
      DigitalStorages.PetaBit,
      DigitalStorages.Byte,
      DigitalStorages.KiloByte,
      DigitalStorages.MegaByte,
      DigitalStorages.GigaByte,
      DigitalStorages.TeraByte,
      DigitalStorages.PetaByte,
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case DigitalStorages.Bit:
            this.onSelectBit(data);
            break;
          case DigitalStorages.KiloBit:
            this.onSelectKiloBit(data);
            break;
          case DigitalStorages.MegaBit:
            this.onSelectMegaBit(data);
            break;
          case DigitalStorages.GigaBit:
            this.onSelectGigaBit(data);
            break;
          case DigitalStorages.TeraBit:
            this.onSelectTeraBit(data);
            break;
          case DigitalStorages.PetaBit:
            this.onSelectPetaBit(data);
            break;
          case DigitalStorages.Byte:
            this.onSelectByte(data);
            break;
          case DigitalStorages.KiloByte:
            this.onSelectKiloByte(data);
            break;
          case DigitalStorages.MegaByte:
            this.onSelectMegaByte(data);
            break;
          case DigitalStorages.GigaByte:
            this.onSelectGigaByte(data);
            break;
          case DigitalStorages.TeraByte:
            this.onSelectTeraByte(data);
            break;
          case DigitalStorages.PetaByte:
            this.onSelectPetaByte(data);
            break;
          default:
            break;
        }
      }
    }
  }

  private onSelectBit(data: ISelection): void {
    // divide the data transfer rate value by 1000
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 1000 });

    // divide the digital storage value by 1e+6
    // this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 1e+9
    // this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 1e+12
    // this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 1e+15
    // this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 8
    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 8 });

    // divide the digital storage value by 8000
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 8000 });

    // divide the digital storage value by 8e+6
    // this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 8e+9
    // this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 8e+12
    // this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 8e+15
    // this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectKiloBit(data: ISelection): void {
    // multiply the digital storage value by 1000
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber * 1000 });

    // divide the digital storage value by 1000
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 1000 });

    // divide the digital storage value by 1e+6
    // this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 1e+9
    // this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 1e+12
    // this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    // multiply the digital storage value by 125
    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber * 125 });

    // divide the digital storage value by 8
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 8 });

    // divide the digital storage value by 8000
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 8000 });

    // divide the digital storage value by 8e+6
    // this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 8e+9
    // this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });

    // divide the digital storage value by 8e+12
    // this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectMegaBit(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectGigaBit(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectTeraBit(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectPetaBit(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectByte(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectKiloByte(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectMegaByte(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectGigaByte(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectTeraByte(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaByte, value: data.inputNumber / 2.59 });
  }

  private onSelectPetaByte(data: ISelection): void {
    this.results.push({ key: DigitalStorages.Bit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraBit, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.PetaBit, value: data.inputNumber / 2.59 });

    this.results.push({ key: DigitalStorages.Byte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.KiloByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.MegaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.GigaByte, value: data.inputNumber / 2.59 });
    this.results.push({ key: DigitalStorages.TeraByte, value: data.inputNumber / 2.59 });
  }

}
