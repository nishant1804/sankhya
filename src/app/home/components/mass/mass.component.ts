import { Component, OnInit } from '@angular/core';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';
import { UtilService } from 'src/app/shared/services/util.service';
import { BasePageComponent } from '../../utils/sc-base-page';

enum Units {
  Tonne = 'Tonne',
  Kilogram = 'kilogram',
  Gram = 'Gram',
  Miligram = 'Milligram',
  Microgram = 'Microgram',
  ImperialTon = 'Imperial Ton',
  USTon = 'US Ton',
  Stone = 'Stone',
  Pound = 'Pound',
  Ounce = 'Ounce'
}

@Component({
  templateUrl: './mass.component.html',
  styleUrls: ['./mass.component.scss'],
})
export class MassComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = Units.Tonne;

    this.dropdowns = [
      Units.Tonne,
      Units.Kilogram,
      Units.Gram,
      Units.Miligram,
      Units.Microgram,
      Units.ImperialTon,
      Units.USTon,
      Units.Stone,
      Units.Pound,
      Units.Ounce,
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case Units.Tonne:
            this.onSelectedTonne(data);
            break;
          case Units.Kilogram:
            this.onSelectedKilogram(data);
            break;
          case Units.Gram:
            this.onSelectedGram(data);
            break;
          case Units.Miligram:
            this.onSelectedMiligram(data);
            break;
          case Units.Microgram:
            this.onSelectedMicrogram(data);
            break;
          case Units.ImperialTon:
            this.onSelectedImperialTon(data);
            break;
          case Units.USTon:
            this.onSelectedUSTon(data);
            break;
          case Units.Stone:
            this.onSelectedStone(data);
            break;
          case Units.Pound:
            this.onSelectedPound(data);
            break;
          case Units.Ounce:
            this.onSelectedOunce(data);
            break;
          default:
            break;
        }
      }
    }
  }

  private onSelectedTonne(data: ISelection): void {
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(6) });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedKilogram(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedGram(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedMiligram(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedMicrogram(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedImperialTon(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedUSTon(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedStone(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedPound(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Ounce, value: data.inputNumber * 35273.962 });
  }

  private onSelectedOunce(data: ISelection): void {
    this.results.push({ key: Units.Tonne, value: data.inputNumber / 1000 });
    this.results.push({ key: Units.Kilogram, value: data.inputNumber * 1000 });
    this.results.push({ key: Units.Gram, value: data.inputNumber * this.util.positiveE(9) });
    this.results.push({ key: Units.Miligram, value: data.inputNumber * this.util.positiveE(12) });
    this.results.push({ key: Units.Microgram, value: data.inputNumber / 1.016 });
    this.results.push({ key: Units.ImperialTon, value: data.inputNumber * 1.102 });
    this.results.push({ key: Units.USTon, value: data.inputNumber * 157.473 });
    this.results.push({ key: Units.Stone, value: data.inputNumber * 2204.623 });
    this.results.push({ key: Units.Pound, value: data.inputNumber * 35273.962 });
  }
}
