import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MassComponent } from './mass.component';

describe('MassComponent', () => {
  let component: MassComponent;
  let fixture: ComponentFixture<MassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MassComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
