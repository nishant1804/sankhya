import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from '../../utils/sc-base-page';
import { UtilService } from 'src/app/shared/services/util.service';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';

enum Energy {
  Joule = 'Joule',
  KiloJoule = 'KiloJoule',
  GramCalorie = 'Gram Calorie',
  KiloCalorie = 'Kilo Calorie',
  WattHour = 'Watt Hour',
  KiloWattHour = 'Kilo Watt Hour',
  ElectronVolt = 'Electron Volt',
  BritishTermalUnit = 'British Termal Unit',
  UsTherm = 'US Therm',
  FootPound = 'Foot-Pound',
}
@Component({
  templateUrl: './energy.component.html',
  styleUrls: ['./energy.component.scss'],
})
export class EnergyComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = Energy.Joule;

    this.dropdowns = [
      Energy.Joule,
      Energy.KiloJoule,
      Energy.GramCalorie,
      Energy.KiloCalorie,
      Energy.WattHour,
      Energy.KiloWattHour,
      Energy.ElectronVolt,
      Energy.BritishTermalUnit,
      Energy.UsTherm,
      Energy.FootPound,
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case Energy.Joule:
            this.onSelectJoule(data);
            break;
          case Energy.KiloJoule:
            this.onSelectKiloJoule(data);
            break;
          case Energy.GramCalorie:
            this.onSelectGramCalorie(data);
            break;
          case Energy.KiloCalorie:
            this.onSelectKiloCalorie(data);
            break;
          case Energy.WattHour:
            this.onSelectWattHour(data);
            break;
          case Energy.KiloWattHour:
            this.onSelectKiloWattHour(data);
            break;
          case Energy.ElectronVolt:
            this.onSelectElectronVolt(data);
            break;
          case Energy.BritishTermalUnit:
            this.onSelectBritishTermalUnit(data);
            break;
          case Energy.UsTherm:
            this.onSelectUsTherm(data);
            break;
          case Energy.FootPound:
            this.onSelectFootPound(data);
            break;
          default:
            break;
        }
      }
    }
  }

  private onSelectJoule(data: ISelection): void {
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectKiloJoule(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectGramCalorie(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectKiloCalorie(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectWattHour(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectKiloWattHour(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectElectronVolt(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectBritishTermalUnit(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectUsTherm(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.FootPound, value: data.inputNumber / 2.59 });
  }

  private onSelectFootPound(data: ISelection): void {
    this.results.push({ key: Energy.Joule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloJoule, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.GramCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloCalorie, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.WattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.KiloWattHour, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.ElectronVolt, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.BritishTermalUnit, value: data.inputNumber / 2.59 });
    this.results.push({ key: Energy.UsTherm, value: data.inputNumber / 2.59 });
  }
}
