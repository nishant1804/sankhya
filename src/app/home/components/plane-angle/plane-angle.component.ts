import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from '../../utils/sc-base-page';
import { UtilService } from 'src/app/shared/services/util.service';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';

@Component({
  templateUrl: './plane-angle.component.html',
  styleUrls: ['./plane-angle.component.scss'],
})
export class PlaneAngleComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = null;

    this.dropdowns = [

    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case 'case':
            break;

          default:
            break;
        }
      }
    }
  }

}
