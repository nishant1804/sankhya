import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from '../../utils/sc-base-page';
import { UtilService } from 'src/app/shared/services/util.service';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';

enum DataTransferRates {
  Bit = 'Bit',
  KiloBit = 'Kilo Bit',
  KiloByte = 'Kilo Byte',
  KibiBit = 'Kilo bi Bit',
  MegaBit = 'Mega Bit',
  MegaByte = 'Mega Byte',
  MegabiBit = 'Mega bi Bit',
  GigaBit = 'Giga Bit',
  GigaByte = 'Giga Byte',
  GigabiBit = 'Giga bi Bit',
  TeraBit = 'Tera Bit',
  TeraByte = 'Tera Byte',
  TearbiBit = 'Tera bi Bit',
}
@Component({
  templateUrl: './data-transfer-rate.component.html',
  styleUrls: ['./data-transfer-rate.component.scss'],
})
export class DataTransferRateComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = null;

    this.dropdowns = [

    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case 'case':
            break;

          default:
            break;
        }
      }
    }
  }

}
