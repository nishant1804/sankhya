import { Component, OnInit } from '@angular/core';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';
import { UtilService } from 'src/app/shared/services/util.service';
import { BasePageComponent } from '../../utils/sc-base-page';

enum AreaTypes {
  SQ_KM = 'Square Kilometer',
  SQ_M = 'Square Meter',
  SQ_Mile = 'Square Mile',
  SQ_Yard = 'Square Yard',
  SQ_Foot = 'Square Foot',
  SQ_Inch = 'Square Inch',
  Hectare = 'Hectare',
  Acre = 'Acre',
}

@Component({
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss'],
})
export class AreaComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = AreaTypes.SQ_KM;

    this.dropdowns = [
      AreaTypes.SQ_KM,
      AreaTypes.SQ_M,
      AreaTypes.SQ_Mile,
      AreaTypes.SQ_Yard,
      AreaTypes.SQ_Foot,
      AreaTypes.SQ_Inch,
      AreaTypes.Hectare,
      AreaTypes.Acre,
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case AreaTypes.SQ_KM:
            this.onSelectKiloMeter(data);
            break;
          case AreaTypes.SQ_M:
            this.onSelectMeter(data);
            break;
          case AreaTypes.SQ_Mile:
            this.onSelectMile(data);
            break;
          case AreaTypes.SQ_Yard:
            this.onSelectYard(data);
            break;
          case AreaTypes.SQ_Foot:
            this.onSelectFoot(data);
            break;
          case AreaTypes.SQ_Inch:
            this.onSelectInch(data);
            break;
          case AreaTypes.Hectare:
            this.onSelectHectare(data);
            break;
          case AreaTypes.Acre:
            this.onSelectAcre(data);
            break;
          default:
            break;
        }
      }
    }
  }

  private onSelectKiloMeter(data: ISelection): void {
    // multiply the area value by 1e+6
    this.results.push({ key: AreaTypes.SQ_M, value: data.inputNumber * this.util.positiveE(6) });

    // divide the area value by 2.59
    this.results.push({ key: AreaTypes.SQ_Mile, value: data.inputNumber / 2.59 });

    // multiply the area value by 1.196e+6
    // this.results.push({ key: AreaTypes.SQ_Yard, value: data.inputNumber * this.util.positiveE(6) });

    // for an approximate result, multiply the area value by 1.076e+7
    // this.results.push({ key: AreaTypes.SQ_Foot, value: data.inputNumber * this.util.positiveE(6) });

    // multiply the area value by 1.55e+9
    // this.results.push({ key: AreaTypes.SQ_Inch, value: data.inputNumber * this.util.positiveE(6) });

    // multiply the area value by 100
    this.results.push({ key: AreaTypes.Hectare, value: data.inputNumber * 100 });

    // for an approximate result, multiply the area value by 247.105
    this.results.push({ key: AreaTypes.Acre, value: data.inputNumber * 247.105 });
  }

  private onSelectMeter(data: ISelection): void {
    // divide the area value by 1e+6
    // this.results.push({ key: AreaTypes.SQ_KM, value: data.inputNumber * this.util.positiveE(6) });

    // divide the area value by 2.59e+6
    // this.results.push({ key: AreaTypes.SQ_Mile, value: data.inputNumber / 2.59 });

    // multiply the area value by 1.196
    this.results.push({ key: AreaTypes.SQ_Yard, value: data.inputNumber * 1.196 });

    // multiply the area value by 10.764
    this.results.push({ key: AreaTypes.SQ_Foot, value: data.inputNumber * 10.764 });

    // multiply the area value by 1550.003
    this.results.push({ key: AreaTypes.SQ_Inch, value: data.inputNumber * 1550.003 });

    // divide the area value by 10000
    this.results.push({ key: AreaTypes.Hectare, value: data.inputNumber / 10000 });

    // for an approximate result, divide the area value by 4046.856
    this.results.push({ key: AreaTypes.Acre, value: data.inputNumber / 4046.856 });
  }

  private onSelectMile(data: ISelection): void {
    // multiply the area value by 2.59
    this.results.push({ key: AreaTypes.SQ_KM, value: data.inputNumber * 2.59 });

    // multiply the area value by 2.59e+6
    // this.results.push({ key: AreaTypes.SQ_M, value: data.inputNumber / 2.59 });

    // for an approximate result, multiply the area value by 3.098e+6
    // this.results.push({ key: AreaTypes.SQ_Yard, value: data.inputNumber * this.util.positiveE(6) });

    // for an approximate result, multiply the area value by 2.788e+7
    // this.results.push({ key: AreaTypes.SQ_Foot, value: data.inputNumber * this.util.positiveE(6) });

    // for an approximate result, multiply the area value by 4.014e+9
    // this.results.push({ key: AreaTypes.SQ_Inch, value: data.inputNumber * this.util.positiveE(6) });

    // for an approximate result, multiply the area value by 258.999
    this.results.push({ key: AreaTypes.Hectare, value: data.inputNumber * 258.999 });

    // multiply the area value by 640
    this.results.push({ key: AreaTypes.Acre, value: data.inputNumber * 640 });
  }

  private onSelectYard(data: ISelection): void {
    // divide the area value by 1.196e+6
    // this.results.push({ key: AreaTypes.SQ_KM, value: data.inputNumber * this.util.positiveE(6) });

    // divide the area value by 1.196
    this.results.push({ key: AreaTypes.SQ_M, value: data.inputNumber / 1.196 });

    // for an approximate result, divide the area value by 3.098e+6
    // this.results.push({ key: AreaTypes.SQ_Mile, value: data.inputNumber * this.util.positiveE(6) });

    // multiply the area value by 9
    this.results.push({ key: AreaTypes.SQ_Foot, value: data.inputNumber * 9 });

    // multiply the area value by 1296
    this.results.push({ key: AreaTypes.SQ_Inch, value: data.inputNumber * 1296 });

    // for an approximate result, divide the area value by 11959.9
    this.results.push({ key: AreaTypes.Hectare, value: data.inputNumber / 11959.9 });

    // divide the area value by 4840
    this.results.push({ key: AreaTypes.Acre, value: data.inputNumber / 4840 });
  }

  private onSelectFoot(data: ISelection): void {
    // for an approximate result, divide the area value by 1.076e+7
    // this.results.push({ key: AreaTypes.SQ_KM, value: data.inputNumber * this.util.positiveE(6) });

    // divide the area value by 10.764
    this.results.push({ key: AreaTypes.SQ_M, value: data.inputNumber / 10.764 });

    // for an approximate result, divide the area value by 2.788e+7
    // this.results.push({ key: AreaTypes.SQ_Mile, value: data.inputNumber * this.util.positiveE(6) });

    // divide the area value by 9
    this.results.push({ key: AreaTypes.SQ_Yard, value: data.inputNumber / 9 });

    // multiply the area value by 144
    this.results.push({ key: AreaTypes.SQ_Inch, value: data.inputNumber * 144 });

    // for an approximate result, divide the area value by 107639.104
    this.results.push({ key: AreaTypes.Hectare, value: data.inputNumber / 107639.104 });

    // divide the area value by 43560
    this.results.push({ key: AreaTypes.Acre, value: data.inputNumber / 43560 });
  }

  private onSelectInch(data: ISelection): void {
    // divide the area value by 1.55e+9
    // this.results.push({ key: AreaTypes.SQ_KM, value: data.inputNumber * this.util.positiveE(6) });

    // divide the area value by 1550.003
    this.results.push({ key: AreaTypes.SQ_M, value: data.inputNumber / 1550.003 });

    // for an approximate result, divide the area value by 4.014e+9
    // this.results.push({ key: AreaTypes.SQ_Mile, value: data.inputNumber * this.util.positiveE(6) });

    // divide the area value by 1296
    this.results.push({ key: AreaTypes.SQ_Yard, value: data.inputNumber / 1296 });

    // divide the area value by 144
    this.results.push({ key: AreaTypes.SQ_Foot, value: data.inputNumber / 144 });

    // divide the area value by 1.55e+7
    // this.results.push({ key: AreaTypes.Hectare, value: data.inputNumber * 100 });

    // for an approximate result, divide the area value by 6.273e+6
    // this.results.push({ key: AreaTypes.Acre, value: data.inputNumber * 247.105 });
  }

  private onSelectHectare(data: ISelection): void {
    // divide the area value by 100
    this.results.push({ key: AreaTypes.SQ_KM, value: data.inputNumber / 100 });

    // multiply the area value by 10000
    this.results.push({ key: AreaTypes.SQ_M, value: data.inputNumber * 10000 });

    // for an approximate result, divide the area value by 258.999
    this.results.push({ key: AreaTypes.SQ_Mile, value: data.inputNumber / 258.999 });

    // for an approximate result, multiply the area value by 11959.9
    this.results.push({ key: AreaTypes.SQ_Yard, value: data.inputNumber * 11959.9 });

    // for an approximate result, multiply the area value by 107639.104
    this.results.push({ key: AreaTypes.SQ_Foot, value: data.inputNumber * 107639.104 });

    // multiply the area value by 1.55e+7
    // this.results.push({ key: AreaTypes.SQ_Inch, value: data.inputNumber * 100 });

    // multiply the area value by 2.471
    this.results.push({ key: AreaTypes.Acre, value: data.inputNumber * 2.471 });
  }

  private onSelectAcre(data: ISelection): void {
    // for an approximate result, divide the area value by 247.105
    this.results.push({ key: AreaTypes.SQ_KM, value: data.inputNumber / 247.105 });

    // for an approximate result, multiply the area value by 4046.856
    this.results.push({ key: AreaTypes.SQ_M, value: data.inputNumber * 4046.856 });

    // divide the area value by 640
    this.results.push({ key: AreaTypes.SQ_Mile, value: data.inputNumber / 640 });

    // multiply the area value by 4840
    this.results.push({ key: AreaTypes.SQ_Yard, value: data.inputNumber * 4840 });

    // multiply the area value by 43560
    this.results.push({ key: AreaTypes.SQ_Foot, value: data.inputNumber * 43560 });

    // for an approximate result, multiply the area value by 6.273e+6
    // this.results.push({ key: AreaTypes.SQ_Inch, value: data.inputNumber * 100 });

    // divide the area value by 2.471
    this.results.push({ key: AreaTypes.Hectare, value: data.inputNumber / 2.471 });
  }
}
