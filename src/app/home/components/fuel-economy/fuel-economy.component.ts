import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from '../../utils/sc-base-page';
import { UtilService } from 'src/app/shared/services/util.service';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';

enum FuelEconomy {
  USMilesPerGallon = 'USMilesPerGallon',
  MilesPerGallon = 'MilesPerGallon',
  KilometerPerLeter = 'Kilometer per Leter',
  LeterPer100Kilometers = 'Leter per 100 Kilometers',
}

@Component({
  templateUrl: './fuel-economy.component.html',
  styleUrls: ['./fuel-economy.component.scss'],
})
export class FuelEconomyComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = FuelEconomy.USMilesPerGallon;

    this.dropdowns = [
      FuelEconomy.USMilesPerGallon,
      FuelEconomy.MilesPerGallon,
      FuelEconomy.KilometerPerLeter,
      FuelEconomy.LeterPer100Kilometers,
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case FuelEconomy.USMilesPerGallon:
            this.onSelectUSMilesPerGallon(data);
            break;
          case FuelEconomy.MilesPerGallon:
            this.onSelectMilesPerGallon(data);
            break;
          case FuelEconomy.KilometerPerLeter:
            this.onSelectKilometerPerLeter(data);
            break;
          case FuelEconomy.LeterPer100Kilometers:
            this.onSelectLeterPer100Kilometers(data);
            break;
          default:
            break;
        }
      }
    }
  }

  private onSelectUSMilesPerGallon(data: ISelection) {
    this.results.push({ key: FuelEconomy.MilesPerGallon, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.KilometerPerLeter, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.LeterPer100Kilometers, value: data.inputNumber / 2.59 });
  }

  private onSelectMilesPerGallon(data: ISelection) {
    this.results.push({ key: FuelEconomy.USMilesPerGallon, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.KilometerPerLeter, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.LeterPer100Kilometers, value: data.inputNumber / 2.59 });
  }

  private onSelectKilometerPerLeter(data: ISelection) {
    this.results.push({ key: FuelEconomy.USMilesPerGallon, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.USMilesPerGallon, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.LeterPer100Kilometers, value: data.inputNumber / 2.59 });
  }

  private onSelectLeterPer100Kilometers(data: ISelection) {
    this.results.push({ key: FuelEconomy.USMilesPerGallon, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.USMilesPerGallon, value: data.inputNumber / 2.59 });
    this.results.push({ key: FuelEconomy.KilometerPerLeter, value: data.inputNumber / 2.59 });
  }

}
