import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from '../../utils/sc-base-page';
import { UtilService } from 'src/app/shared/services/util.service';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';

enum Length {
  Kilometre = 'Kilometre',
  Metre = 'Metre',
  Centimetre = 'Centimetre',
  Milimetre = 'Milimetre',
  Micrometre = 'Micrometre',
  Nanometre = 'Nanometre',
  Mile = 'Mile',
  Yard = 'Yard',
  Foot = 'Foot',
  Inch = 'Inch',
  NauticalMile = 'Nautical Mile',
}
@Component({
  templateUrl: './length.component.html',
  styleUrls: ['./length.component.scss'],
})
export class LengthComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = Length.Kilometre;

    this.dropdowns = [
      Length.Kilometre,
      Length.Metre,
      Length.Centimetre,
      Length.Milimetre,
      Length.Micrometre,
      Length.Nanometre,
      Length.Mile,
      Length.Yard,
      Length.Foot,
      Length.Inch,
      Length.NauticalMile,
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case Length.Kilometre:
            this.onSelectKilometre(data);
            break;
          case Length.Metre:
            this.onSelectMetre(data);
            break;
          case Length.Centimetre:
            this.onSelectCentimetre(data);
            break;
          case Length.Milimetre:
            this.onSelectMilimetre(data);
            break;
          case Length.Micrometre:
            this.onSelectMicrometre(data);
            break;
          case Length.Nanometre:
            this.onSelectNanometre(data);
            break;
          case Length.Yard:
            this.onSelectYard(data);
            break;
          case Length.Mile:
            this.onSelectMile(data);
            break;
          case Length.Foot:
            this.onSelectFoot(data);
            break;
          case Length.Inch:
            this.onSelectInch(data);
            break;
          case Length.NauticalMile:
            this.onSelectNauticalMile(data);
            break;

          default:
            break;
        }
      }
    }
  }

  private onSelectKilometre(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectMetre(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectCentimetre(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectMilimetre(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectMicrometre(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectNanometre(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectYard(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectMile(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectFoot(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectInch(data: ISelection) {
    throw new Error('Method not implemented.');
  }

  private onSelectNauticalMile(data: ISelection) {
    throw new Error('Method not implemented.');
  }

}
