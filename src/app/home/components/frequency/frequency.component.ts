import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from '../../utils/sc-base-page';
import { UtilService } from 'src/app/shared/services/util.service';
import { ISelection } from 'src/app/shared/interfaces/selection.interface';

enum Frequency {
  Hertz = 'Hertz',
  KiloHertz = 'Kilo Hertz',
  MegaHertz = 'Mega Hertz',
  GigaHertz = 'Giga Hertz',
}
@Component({
  templateUrl: './frequency.component.html',
  styleUrls: ['./frequency.component.scss'],
})
export class FrequencyComponent extends BasePageComponent implements OnInit {

  constructor(private util: UtilService) {
    super();
    this.selectedDropdown = Frequency.Hertz;

    this.dropdowns = [
      Frequency.Hertz,
      Frequency.KiloHertz,
      Frequency.MegaHertz,
      Frequency.GigaHertz,
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onChange(data: ISelection): void {
    if (data && !(data instanceof Event)) {
      this.results = [];
      if (data.inputNumber && data.selectedDropdown) {
        switch (data.selectedDropdown) {
          case Frequency.Hertz:
            this.onSelectHertz(data);
            break;
          case Frequency.KiloHertz:
            this.onSelectKiloHertz(data);
            break;
          case Frequency.MegaHertz:
            this.onSelectMegaHertz(data);
            break;
          case Frequency.GigaHertz:
            this.onSelectGigaHertz(data);
            break;

          default:
            break;
        }
      }
    }
  }

  private onSelectHertz(data: ISelection): void {
    this.results.push({ key: Frequency.KiloHertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.MegaHertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.GigaHertz, value: data.inputNumber / 2.59 });
  }

  private onSelectKiloHertz(data: ISelection): void {
    this.results.push({ key: Frequency.Hertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.MegaHertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.GigaHertz, value: data.inputNumber / 2.59 });
  }

  private onSelectMegaHertz(data: ISelection): void {
    this.results.push({ key: Frequency.Hertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.KiloHertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.GigaHertz, value: data.inputNumber / 2.59 });
  }

  private onSelectGigaHertz(data: ISelection): void {
    this.results.push({ key: Frequency.Hertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.KiloHertz, value: data.inputNumber / 2.59 });
    this.results.push({ key: Frequency.MegaHertz, value: data.inputNumber / 2.59 });
  }

}
