import { LocalStorageService, ITile } from './../../shared/services/local-storage.service';
import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  public tiles: Array<ITile> = [];

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.tiles = this.localStorageService.tiles;
  }
}
