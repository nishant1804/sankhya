import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { SettingsComponent } from '../settings/settings.component';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsCanDeactiveGuard implements CanDeactivate<SettingsComponent>  {
  constructor(private localStorageService: LocalStorageService) { }
  canDeactivate(component: SettingsComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    this.localStorageService.tiles = component.tiles;
    return true;
  }
}
