import { TestBed, async, inject } from '@angular/core/testing';

import { SettingsCanDeactiveGuard } from './settings-can-deactive.guard';

describe('SettingsCanDeactiveGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettingsCanDeactiveGuard]
    });
  });

  it('should ...', inject([SettingsCanDeactiveGuard], (guard: SettingsCanDeactiveGuard) => {
    expect(guard).toBeTruthy();
  }));
});
