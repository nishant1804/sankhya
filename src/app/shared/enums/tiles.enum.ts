export enum TileNames {
    Area = 'Area',
    DataTransferRate = 'Data Transfer Rate',
    DigitalStorage = 'Digital Storage',
    Energy = 'Energy',
    Frequency = 'Frequency',
    FuelEconomy = 'Fuel Economy',
    Length = 'Length',
    Mass = 'Mass',
    PlaneAngle = 'Plane Angle',
    Pressure = 'Pressure',
    Speed = 'Speed',
    Temprature = 'Temprature',
    Time = 'Time',
    Volume = 'Volume'
}
