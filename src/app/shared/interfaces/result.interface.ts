export interface IResult {
    key: string;
    value: string | number;
}
