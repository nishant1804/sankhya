import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  public positiveE(num: number): number {
    return Math.pow(10, num);
  }
}
