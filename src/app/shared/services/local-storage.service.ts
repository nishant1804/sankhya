import { Injectable } from '@angular/core';

export interface ITile {
  name: string;
  routerLink?: string;
  icon: string;
  selected: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  public get tiles(): ITile[] {
    return JSON.parse(window.localStorage.getItem('tiles'));
  }

  public set tiles(v: ITile[]) {
    window.localStorage.setItem('tiles', JSON.stringify(v));
  }

  constructor() {
    this.tiles = this.tiles || this.initTiles();
  }

  private initTiles(): ITile[] {
    return [
      {
        name: 'Area',
        icon: 'fa fa-area-chart',
        selected: true
      },
      // {
      //   name: 'Data Transfrer',
      //   icon: 'fa fa-exchange',
      //   selected: true
      // },
      {
        name: 'Digital Storage',
        icon: 'fa fa-archive',
        selected: true
      },
      {
        name: 'Energy',
        icon: 'fa fa-lightbulb-o',
        selected: true
      },
      {
        name: 'Frequency',
        icon: 'fa fa-line-chart',
        selected: true
      },
      {
        name: 'Fuel Economy',
        icon: 'fa fa-adjust',
        selected: true
      },
      {
        name: 'Length',
        icon: 'fa fa-arrows-h',
        selected: true
      },
      {
        name: 'Mass',
        icon: 'fa fa-circle-o',
        selected: true
      },
      {
        name: 'Plane Angle',
        icon: 'fa fa-plane',
        selected: true
      },
      {
        name: 'Pressure',
        icon: 'fa fa-map-pin',
        selected: true
      },
      {
        name: 'Speed',
        icon: 'fa fa-rocket',
        selected: true
      },
      {
        name: 'Temprature',
        icon: 'fa fa-thermometer-full',
        selected: true
      },
      {
        name: 'Time',
        icon: 'fa fa-clock-o',
        selected: true
      },
      {
        name: 'Volume',
        icon: 'fa fa-cube',
        selected: true
      },
    ];
  }
}
