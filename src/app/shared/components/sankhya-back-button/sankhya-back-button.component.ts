import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sc-back-button',
  templateUrl: './sankhya-back-button.component.html',
  styleUrls: ['./sankhya-back-button.component.scss'],
})
export class SankhyaBackButtonComponent implements OnInit {
  @Input() public page: string;

  constructor() { }

  ngOnInit() { }

}
