import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SankhyaContentComponent } from './sankhya-content.component';

describe('SankhyaContentComponent', () => {
  let component: SankhyaContentComponent;
  let fixture: ComponentFixture<SankhyaContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SankhyaContentComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SankhyaContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
