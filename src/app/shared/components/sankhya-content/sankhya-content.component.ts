import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IResult } from 'src/app/shared/interfaces/result.interface';
import { ISelection } from '../../interfaces/selection.interface';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'sc-sankhya-content',
  templateUrl: './sankhya-content.component.html',
  styleUrls: ['./sankhya-content.component.scss'],
  providers: [Clipboard]
})
export class SankhyaContentComponent implements OnInit {
  @Input() public dropdowns: Array<any>;
  @Input() public selectedDropdown: any;
  @Input() public results: Array<IResult>;
  @Input() public inputNumber = 1;

  @Output() change: EventEmitter<ISelection> = new EventEmitter();

  constructor(private clipboard: Clipboard, public toastController: ToastController) { }

  ngOnInit() { }

  public onChange(): void {
    if (this.selectedDropdown !== undefined && this.inputNumber !== undefined) {
      const data: ISelection = { selectedDropdown: this.selectedDropdown, inputNumber: this.inputNumber };
      this.change.emit(data);
    }
  }

  public copy(value: number): void {
    this.clipboard.copy(value.toString());
    this.presentToast();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Copied!',
      duration: 2000
    });
    toast.present();
  }
}
