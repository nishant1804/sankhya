import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { SankhyaBackButtonComponent } from './components/sankhya-back-button/sankhya-back-button.component';
import { SankhyaContentComponent } from './components/sankhya-content/sankhya-content.component';
import { AboutComponent } from './components/about/about.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: 'about',
        component: AboutComponent
      }
    ])
  ],
  declarations: [
    SankhyaBackButtonComponent,
    SankhyaContentComponent,
    AboutComponent
  ],
  exports: [
    SankhyaBackButtonComponent,
    SankhyaContentComponent,
    AboutComponent
  ]
})
export class SharedModule { }
